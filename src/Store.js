import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import AddEditReducer from './Reducers/AddEditReducer'
const reducers = combineReducers({
    AddEditStore:AddEditReducer
  })
  
  const middleware = [thunk]
  
  const store = createStore(
    reducers,
    {},
    applyMiddleware(...middleware),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
  
  export default store
  