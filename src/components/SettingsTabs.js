import React from 'react'
import { Tabs, Tab } from 'react-bootstrap'
import Content from './Tabs/Content';
import Mappings from './Tabs/Mappings';
import Settings from './Tabs/Settings';

import { connect } from 'react-redux'
import { onChange ,addData} from '../actions';

function SettingsTabs(props) {
    return (
        <div>
            <Tabs
                id="controlled-tab-example"
                activeKey={props.AddEditStore.key}
                onSelect={(key) => {props.addData({key})}}>
                <Tab eventKey="Content" title="Content">
                    <Content {...props}/>
                </Tab>
                <Tab eventKey="Mappings" title="Mappings">
                    <Mappings {...props}/>
                </Tab> 
                <Tab eventKey="Settings" title="Settings">
                    <Settings {...props}/>
                </Tab>

            </Tabs>

        </div>
    )
}

SettingsTabs.propTypes = {

}

function mapStateToProps(state) {
    return {
        AddEditStore:state.AddEditStore
    }
  }
  
export default connect(mapStateToProps, {
    onChange,
    addData
  }) (SettingsTabs)

