import React, { Component } from 'react'
import { InputGroup, FormControl } from 'react-bootstrap'

export class Settings extends Component {
    onDrag(color, c) {

        this.props.addData({ color })
    }
    onchange(e) {
        debugger
        let div = document.getElementById(this.props.AddEditStore.id);
        if (div) {
            div.style = div.getAttribute('style') + "width:" + e.target.value + 'px'
        }

    }
    render() {
        return (
            <div>
                General Settings
            {this.props.AddEditStore.draggedElement = '' && <div>
                    <div>
                        <div style={{ width: '100%' }}>
                            <label>Area width</label>
                            <label style={{ textAlign: 'right' }}></label>
                        </div>
                        <div>
                            <InputGroup className="mb-3">
                                <FormControl aria-label="Area width" name='width' onChange={this.onchange.bind(this)} />
                                <InputGroup.Append>
                                    <InputGroup.Text >px</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </div>
                    <div>
                        <div style={{ width: '100%' }}>
                            <label>Area height</label>
                            <label style={{ textAlign: 'right' }}></label>
                        </div>
                        <div>
                            <InputGroup className="mb-3">
                                <FormControl aria-label="Area height" name='height' onChange={onchange} />
                                <InputGroup.Append>
                                    <InputGroup.Text >px</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </div>
                    <div>
                        <div style={{ width: '100%' }}>
                            <label>Background color</label>
                            {/* <ColorPicker value={props.AddEditStore.color || 'red'} onDrag={onchange} onChange={onDrag} /> */}
                            {this.props.AddEditStore.color}
                        </div>
                    </div>
                </div>
                }
                {this.props.AddEditStore.draggedElement = 'Carousels' &&
                    <div>
                        <div>
                            <div style={{ width: '100%' }}>
                                <label>No of Slides</label>
                                <label style={{ textAlign: 'right' }}></label>
                            </div>
                            <InputGroup className="mb-3">
                                <FormControl aria-label="Area width" name='width' />
                            </InputGroup>
                        </div>
                    </div>}
            </div>
        )
    }
}

export default Settings
