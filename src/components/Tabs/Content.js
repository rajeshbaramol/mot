import React from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import data from '../../data';

function Content(props) {

    function handleClick() {
        //props.addData({'html':data.Carousels})

    }
    
    document.addEventListener('dragstart', function (event) {
        event.dataTransfer.setData("data", data.Carousels);
        props.onChange({draggedElement:'Carousels'})
    });
    document.addEventListener("dragover", function (event) {
        event.preventDefault();
    });
    return (
        <div>
            <Container>
                <Row>
                    <Col draggable>
                        <div style={{ width: '150px', marginTop: '2%', height: '100px', border: '1px solid black', placeSelf: 'center' }} >
                            <p style={{ placeSelf: 'center' }}> Carousels</p>
                        </div>
                    </Col>
                    <Col>
                        <div style={{ width: '150px', marginTop: '2%', height: '100px', border: '1px solid black', placeSelf: 'center' }}>
                            <p style={{ placeSelf: 'center' }}> Text</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div style={{ width: '150px', marginTop: '2%', height: '100px', border: '1px solid black', placeSelf: 'center' }} onClick={handleClick}>
                            <p style={{ placeSelf: 'center' }}> Button</p>
                        </div>
                    </Col>
                    <Col>
                        <div style={{ width: '150px', marginTop: '2%', height: '100px', border: '1px solid black', placeSelf: 'center' }}>
                            <p style={{ placeSelf: 'center' }}> Image</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

Content.propTypes = {

}

export default Content

