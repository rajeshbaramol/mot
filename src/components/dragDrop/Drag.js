import React, { Component } from 'react'

export class Drag extends Component {
    drag(ev){
        ev.dataTransfer.setData("html", 'props.dataItem');
    }
    render() {
        return (
            <div draggable onDragStart={this.drag}>
                
            </div>
        )
    }
}

export default Drag
