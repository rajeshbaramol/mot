import React, { Component } from 'react';
import { Col, Row, Card } from 'react-bootstrap';

class TemplateList extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <Card style={{ width: '14rem' }}>
                        <Card.Img variant="top" src="https://img.icons8.com/all/500/add.png" />
                            <Card.Body>
                                <Card.Title>Add New</Card.Title>
                            </Card.Body>
                        </Card>
                    </Col>

                </Row>
            </div>
        );
    }
}

TemplateList.propTypes = {

};

export default TemplateList;