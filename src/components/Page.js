import React, { Component } from 'react';
import { connect } from 'react-redux'
import { onChange, addData } from '../actions';
import AddEditTemplate from './AddEditTemplate';
import { Row, Col } from 'react-bootstrap';
import SettingsTabs from './SettingsTabs';


class Page extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }

    }

    render() {
        return (
            <div>
                <Row style={{ margin: 'auto' }}>
                    <Col xs={11} md={8} sm={8} lg={8} xl={9}></Col> <Col xs={11} md={8} sm={8} lg={8} xl={9} className='workSpace'>
                        <AddEditTemplate {...this.props}></AddEditTemplate>
                    </Col>
                    <Col>
                        <SettingsTabs></SettingsTabs>
                    </Col>
                </Row>
            </div>
        );
    }
}

Page.propTypes = {

};
function mapStateToProps(state) {
    return {
        AddEditStore: state.AddEditStore
    }
}

export default connect(mapStateToProps, {
    onChange,
    addData
})
    (Page);