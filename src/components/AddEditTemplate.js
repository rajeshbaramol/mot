import React, { } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function AddEditTemplate(props) {
    function handleClick(e) {
        props.addData({ id: e.target.id })
        if (e.target.id) {
          let div=  document.getElementById(e.target.id);
          div.style="border:3px solid black"
        }
    }

    document.addEventListener("drop", function (event) {
        let data = event.dataTransfer.getData('data')
        let innehtml = document.getElementById('#root#');
        innehtml.innerHTML = data
        let html = innehtml.outerHTML
        props.addData({ html })
    });
    document.addEventListener("dragover", function (event) {
        event.preventDefault();
    });


    let section = document.getElementById(props.AddEditStore.id)
    if (section && section.type === 'file') {
        section.onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                debugger
                console.log(props.AddEditStore.id)
                section.parentElement.innerHTML = '<img id="img1" src="' + e.target.result + '"/>'
            }
            reader.readAsDataURL(section.files[0])
        }

    }
    document.addEventListener("handleImageSelector", function () {
        //event.preventDefault();
        debugger
    });
    return (
        <div>
            <div dangerouslySetInnerHTML={{ __html: props.AddEditStore.html }} onClick={handleClick} >
            </div>
        </div>
    );
}

AddEditTemplate.propTypes = {

};

export default AddEditTemplate;
