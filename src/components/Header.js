import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { connect } from 'react-redux'


export class Header extends Component {
  handleClick(){
    debugger
    const element = document.createElement("a");
    const file = new Blob([this.props.AddEditStore.html], {type: 'text/plain'});
    element.href = URL.createObjectURL(file);
    element.download = "myFile.html";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }
    render() {
        return (
            <div className='header'>
            <Button style={{float:'right',margin:'auto'}} onClick={this.handleClick.bind(this)}>save</Button>
          </div>
        )
    }
}

function mapStateToProps(state) {
  return {
      AddEditStore:state.AddEditStore
  }
}

export default connect(mapStateToProps, {
})
(Header);
