import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Page from './components/Page';
import { Provider } from 'react-redux'
import store from './Store'

function App() {
  return (
    <Provider store={store}>
      <Header></Header>
      <Page></Page>

    </Provider>
  );
}

export default App;
