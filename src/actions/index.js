
import * as actionTypes from '../actionTypes';

export const onChange = data => ({
    type: actionTypes.ON_CHANGE,
    payload: data
  })

  export const addData = data => ({
    type: actionTypes.ADD_DATA,
    payload: data
  })
  